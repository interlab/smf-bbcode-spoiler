<?php

defined('SMF') or die('Hacking attempt...');

// SMF 2.0.2 Subs.php call_integration_hook('integrate_bbc_codes', array(&$codes));
function spoiler_bbc_codes(&$codes)
{
	global $txt;

	$ary = [
		[
			'tag' => 'spoiler',
			'block_level' => true,
			'before' => '<div class="sp-wrap"><div class="sp-body" title="' . $txt['spoiler_title'] . '">',
			'after' => '</div></div>',
		],
		[
			'tag' => 'spoiler',
			'type' => 'unparsed_equals', // парсить заголовок запрещено
			'validate' => function(&$tag, &$data, $disabled) {
				global $txt;
				if (empty($data))
					$data = $txt['spoiler_title'];
					// $data = str_replace(array("[", "]"), array("&#0091;", "&#0093;", $data));
			},
			// 'test' => '[#]?([A-Za-z][A-Za-z0-9_\-]*)\]',
			'block_level' => true,
			'before' => '<div class="sp-wrap"><div class="sp-body" title="$1">',
			'after' => '</div></div>',
	]];
	$codes = array_merge($codes, $ary);
}

// SMF 2.0.2 Load.php call_integration_hook('integrate_load_theme');
function spoiler_load_theme()
{
	global $context, $settings;

	loadLanguage('Spoiler');

	$context['html_headers'] .= '
	<link rel="stylesheet" type="text/css" href="' . $settings['default_theme_url'] . '/css/spoiler/spoiler.css" />
	<script type="text/javascript"><!-- // --><![CDATA[
		window.jQuery || document.write(\'<script type="text/javascript" src="' . $settings['default_theme_url'] . '/scripts/spoiler/jquery-1.11.2.min.js"><\/script>\');
	// ]]></script>
	<script src="' . $settings['default_theme_url'] . '/scripts/spoiler/spoiler.js" type="text/javascript"></script>';
}

// SMF 2.0.2 Subs-Editor.php call_integration_hook('integrate_bbc_buttons', array(&$context['bbc_tags']));
function spoiler_bbc_buttons(&$bbc_tags)
{
	global $txt;

	$bbc_tags[count($bbc_tags) - 1][] = [
		'image' => 'spoiler',
		'code' => 'spoiler',
		'before' => '[spoiler]',
		'after' => '[/spoiler]',
		'description' => $txt['bbc_spoiler']
	];
}
