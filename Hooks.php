﻿<?php

// Handle running this file by using SSI.php
if (!defined('SMF') && file_exists(dirname(__FILE__) . '/SSI.php'))
	require_once(dirname(__FILE__) . '/SSI.php');
elseif (!defined('SMF'))
	die('<b>Error:</b> Cannot install - please verify you put this in the same place as SMF\'s index.php.');
elseif ((SMF == 'SSI') && !$user_info['is_admin'])
	die('Admin privileges required.');

$hooks = [
	'integrate_pre_include' => '$boarddir/Sources/Spoiler-Subs.php',
	'integrate_bbc_codes' => 'spoiler_bbc_codes',
	'integrate_load_theme' => 'spoiler_load_theme',
	'integrate_bbc_buttons' => 'spoiler_bbc_buttons',
];

if (!empty($context['uninstalling']))
	$call = 'remove_integration_function';
else
	$call = 'add_integration_function';

foreach ($hooks as $hook => $function)
{
	$call($hook, $function);
}
